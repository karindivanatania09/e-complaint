<?php

namespace Database\Seeders;

use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Hash;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        \App\Models\User::create([
            'username' => 'Karin',
            'password' => Hash::make('karin_123'),
            'phone' => '089670029340',
            'gender' => 'Female',
            'email' => 'karindivana12@gmail.com',
            'level' => 'Administrator',
        ]);

        \App\Models\User::create([
            'username' => 'Levin',
            'password' => Hash::make('coba_300'),
            'phone' => '0895703330',
            'gender' => 'male',
            'email' => 'levinlevin@gmail.com',
            'level' => 'Officers',
        ]);

        \App\Models\User::create([
            'username' => 'Marsella',
            'password' => Hash::make('gracia'),
            'phone' => '0896546221',
            'gender' => 'Female',
            'email' => 'marsella@gmail.com',
            'level' => 'Student',
        ]);
    }
}
