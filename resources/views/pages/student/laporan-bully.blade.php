
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    @include('template.head')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
@include('template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
@include('template.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="width: 1800px">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Laporan bully</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Generate Lporan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="card card-info card-outline">
            <div class="card-header">
                <div class="card-tools">
                  <button onclick="window.print()">Print</button>
                    <a href="{{route('create-pengaduan')}}" class="btn btn-success">lapor<i class="fas fa-plus-square"></i></a>
                </div>
            </div>

    <div class="card-body" style="width: 1000px">
          <table class="table table-bordered">
                  <tr>
                      <th>id</th>
                      <th>school_class</th>
                      <th>report_title</th>
                      <th>report_detail</th>
                      <th>incident_time</th>
                      <th>place</th>
                      <th>type_of_bullying</th>
                      <th>victim_name</th>
                      <th>class</th>
                      <th>reporter_name</th>
                      <th>proof</th>
                      <th>photo_description</th>
                      <th>responses</th>
                      <th class="d-print-none">verification</th>
                  </tr>
                  @foreach ($complaint as $i => $sis)
                      <tr>
                          <td>{{ $sis->id }}</td>
                          <td>{{ $sis->school_class }}</td>
                          <td>{{ $sis->report_title }}</td>
                          <td>{{ $sis->report_detail }}</td>
                          <td>{{ date('d-m-y', strtotime($sis->incident_time)) }}</td>
                          <td>{{ $sis->place }}</td>
                          <td>{{ $sis->type_of_bullying }}</td>
                          <td>{{ $sis->victim_name }}</td>
                          <td>{{ $sis->class }}</td>
                          <td>{{ $sis->reporter_name }}</td>
                          <td>{{ $sis->proof }}</td>
                          <td>{{ $sis->photo_description }}</td>
                          <td> <a href="{{ route('edit-siswa', $sis->id)}}" class="btn btn-secondary">{{ $sis->responses }}</a></td>
                          <td>
                            <a href="{{ url('', $sis->id)}}" class="btn btn-primary d-print-none">{{ $sis->verification }}</a>
                          </td>
                          
                           
                            
                            </form>
                         
                      </tr>
                  @endforeach
          </table>
      </div>
  </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h1>Ini halaman data user</h1>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
@include('template.footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
@include('sweetalert::alert')

<!-- jQuery -->
<script src="{{asset('AdminLTE/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE/dist/js/adminlte.min.js"></script')}}">
</body>
</html>
