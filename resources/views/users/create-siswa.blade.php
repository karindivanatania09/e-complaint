
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    @include('template.head')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
@include('template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
@include('template.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Halaman data siswa</h1>
          </div><!-- /.col -->

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
      <div class="content">
        <div class="card card-info card-outline">
          <div class="card-header">
            <h3>Tambah Siswa</h3>
          </div>
          <div class="card-body">
            <form action="{{ route('simpan-siswa')}}" method="post">
              {{ csrf_field()}}
              <div class="form-group">
                <input type="text" id="username" name="username" class="form-control" placeholder="Username"> 
                </div>
              <div class="form-group">
                <input type="text" id="password" name="password" class="form-control" placeholder="password"> 
                </div>
                <div class="form-group">
                  <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone"> 
                  </div>
                  <div class="form-group">
                    <select name="gender" class="form-control">
                       <option value="">Gender</option>
                       @foreach (["Male", "Female"] as $item)
                       <option value="{{ $item}}">{{ $item}}</option>
                       @endforeach
                    </select>
                </div>
                <div class="form-group">
                  <input type="text" id="email" name="email" class="form-control" placeholder="Email"> 
                  </div>
                  <div class="form-group">
                    <select name="level" class="form-control">
                       <option value="">Level</option>
                       @foreach (["Administrator", "Officers", "Student"] as $item)
                       <option value="{{ $item }}">{{ $item}}</option>
                       @endforeach
                    </select>
                </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
              </form> 
            </div>
        </div>
      </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
@include('template.footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{asset('AdminLTE/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE/dist/js/adminlte.min.js"></script')}}">
</body>
</html>
