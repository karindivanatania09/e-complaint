
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    @include('template.head')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
@include('template.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
@include('template.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="width: 1500px;">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Halaman data siswa</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Siswa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="card card-info card-outline">
            <div class="card-header">
                <div class="card-tools">
                    <button onclick="window.print()">Print</button>
                    <a href="{{route('create-siswa')}}" class="btn btn-success">Tambah Data <i class="fas fa-plus-square"></i></a>
                </div>
            </div>

            
            

    <div class="card-body">
          <table class="table table-bordered" id="user">
                  <tr>
                      <th>id</th>
                      <th>Username</th>
                      <th>Password</th>
                      <th>Phone</th>
                      <th>Gender</th>
                      <th>email</th>
                      <th>level</th>
                      <th>action</th>
                  </tr>
                  @foreach ($user as $i => $sis)
                      <tr>
                          <td>{{ $sis->id }}</td>
                          <td>{{ $sis->username }}</td>
                          <td>{{ $sis->password }}</td>
                          <td>{{ $sis->phone }}</td>
                          <td>{{ $sis->gender }}</td>
                          <td>{{ $sis->email }}</td>
                          <td>{{ $sis->level }}</td>
                          <td>
                            <a href="{{ route('edit-siswa', $sis->id)}}" class="btn btn-secondary">Edit</a>
                            @method('delete')
                            <a href="{{ url('delete-siswa', $sis->id)}}" class="btn btn-danger">Delete</a>
                            </form>
                          </td>
                      </tr>
                  @endforeach
          </table>
      </div>
    </div>
      
  </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h1>Ini halaman data user</h1>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
@include('template.footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
@include('sweetalert::alert')

<!-- jQuery -->
<script src="{{asset('AdminLTE/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE/dist/js/adminlte.min.js"></script')}}">


</body>
</html>
