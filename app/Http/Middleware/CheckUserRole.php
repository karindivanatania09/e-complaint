<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CheckUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, $role)
    {
        if($role == 'Administrator' && Auth::user()->role != 'Administrator') {
            return redirect()->route('login');
        }
        
        if ($role == 'Officers' && Auth::user()->role !='Officers') {
            return redirect()->route('login');
        }

        if ($role == 'Student' && Auth::user()->role !='Student') {
            return redirect()->route('login');
        }

        if(isset($_POST['Student_login'])) {
            header("Location: halaman-home.blade.php");
        }
        if(isset($_POST['Officers_login'])) {
            header("Location: dashboard.php");
        }
        if(isset($_POST['Administrator_login'])) {
            header("Location: dashboard.php");
        }
        return $next($request);
    }
}
