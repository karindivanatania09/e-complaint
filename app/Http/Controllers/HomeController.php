<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if (Auth::user()->level === 'Administrator') {
            return view('pages.admin.dashboard');
        } elseif (Auth::user()->level === 'Officers') {
            return view('pages.officers.dashboard');
        } elseif (Auth::user()->level === 'Student') {
            return view('pages.student.dashboard');
        }
    }


}
